<!-- Footer -->
<footer class="py-5 bg-dark">
  <div class="container">
  	<!-- Footer can have Copyright or Social Media Links -->
    <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
  </div>
  <!-- /.container -->
</footer>

<script>
let loc = window.location.href;
if(/product/.test(loc)){
   $("footer").addClass("fixed-bottom");
};
</script>