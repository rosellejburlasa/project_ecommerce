@extends('layouts.app')

@section('content')
	<div class="card mb-3 mt-5" style="max-width: 50rem;margin-left: 19rem;">
	  <div class="row no-gutters">
	    <div class="col-md-4">
	      <img src="{{$item->img_path}}">
	    </div>
	    <div class="col-md-8">
	      <div class="card-body">
	        <h5 class="card-title">Product Information</h5>
	        <h4>{{$item->name}}</h4>
	        <p>{{$item->description}}</p>
            <h5>₱ {{number_format($item->price, 2)}}</h5>
          <form method="POST" action="#">
      		@csrf
            <input type="number" class="mb-3 mr-2" value="1" min="1" max="15" name="qty">
            <button class="btn btn-primary" type="submit">
            	<i class="fas fa-cart-plus"></i> Add to Cart
            </button>
          </form>
	      </div>
	    </div>
	  </div>
	</div>
@endsection