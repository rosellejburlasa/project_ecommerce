@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
        {{--First Column--}}
        <div class="col-lg-3">
                <h4 id="catalogue">Collections</h4>
                <div class="list-group">
					@foreach ($categories as $category)
                        <li class="list-group-item">
                            <a href="#" class="list-group-item list-group-item-action text-center">
                            <i class="fas fa-chevron-circle-right"></i> {{ $category->name }}</a>
                        </li>
                    @endforeach
                </div>
            </div>
        {{--Second Column--}}
        <div class="col-lg-9">
		 		<div class="form-group">
			        <div class="form-group">
			          	<form method="GET" action="" class="form-group" role="search">
			          		@csrf
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="search" name="search">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-default input-group-text"><i class="fas fa-search"></i></button>
                                </div>
                            </div>
                        </form>
			        </div>
			      </div>
			    <div class="row">
			    	@if(count($items)>0)
				    	@foreach ($items as $item)
				    		<div class="col-md-4 mb-3">
				              <div class="card h-100">
				                 <img src="{{$item->img_path}}">
			                    <div class="card-body">
			                    <h4><a href="{{route('products.show',$item->id)}}">{{$item->name}}</a></h4>
			                      <h5>₱ {{$item->price}}</h5>
			                      <a href="#" class="btn btn-block btn-primary">Buy Now</a>+
			                    </div>
				              </div>
				            </div>
				    	@endforeach
				    @else
				    	<div class="col col-lg-12 mb-3 d-inline-block">
				    		<div class="text-center mt-5">
				    			<h4>Sorry! Product is not available yet.</h4>
                                <small>Try to browse to other products. </small>
				    		</div>
				    	</div>
				    @endif
			    </div>
            </div>
        </div>
    </div>
@endsection